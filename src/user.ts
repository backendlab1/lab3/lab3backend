import { AppDataSource } from "./data-source";
import { Role } from "./entity/Role";
import { User } from "./entity/User";

AppDataSource.initialize()
  .then(async () => {
    const usersRepository = AppDataSource.getRepository(User);
    const roleRepository = AppDataSource.getRepository(Role);
    const adminRole = await roleRepository.findOneBy({ id: 1 });
    const userRole = await roleRepository.findOneBy({ id: 2 });
    await usersRepository.clear();
    console.log("Inserting a new user into the Memory...");
    var user = new User();
    user.id = 1;
    user.email = "aaa@gmail.com";
    user.gender = "male";
    user.password = "pass1234";
    user.roles = [];
    user.roles.push(adminRole);
    user.roles.push(userRole);

    console.log("Inserting a new user into the Database...");
    await usersRepository.save(user);

    user = new User();
    user.id = 2;
    user.email = "aaa1@gmail.com";
    user.gender = "male";
    user.password = "pass1234";
    user.roles = [];

    user.roles.push(userRole);
    console.log("Inserting a new user into the Database...");
    await usersRepository.save(user);

    user = new User();
    user.id = 3;
    user.email = "aaa1@gmail.com";
    user.gender = "male";
    user.password = "pass1234";
    user.roles = [];

    user.roles.push(userRole);
    console.log("Inserting a new user into the Database...");
    await usersRepository.save(user);

    const users = await usersRepository.find({ relations: { roles: true } });
    console.log(JSON.stringify(users, null, 2));
    
    const roles = await roleRepository.find({ relations: { users: true } });
    console.log(JSON.stringify(roles, null, 2));
  })
  .catch((error) => console.log(error));
