import { AppDataSource } from "./data-source";
import { Product } from "./entity/Product";
import { Type } from "./entity/Type";
import { User } from "./entity/User";

AppDataSource.initialize()
  .then(async () => {
    const typesRepository = AppDataSource.getRepository(Type);
    const drinkType = await typesRepository.findOneBy({ id: 1 });
    const bekeryType = await typesRepository.findOneBy({ id: 2 });
    const productRepository = AppDataSource.getRepository(Product);

    await productRepository.clear();

    var products = new Product();
    products.id = 1;
    products.name = "cocacola";
    products.type = drinkType;
    products.price = 50;

    await productRepository.save(products);

    var products = new Product();
    products.id = 2;
    products.name = "cooffee";
    products.type = drinkType;
    products.price = 50;

    await productRepository.save(products);

    var products = new Product();
    products.id = 3;
    products.name = "cake";
    products.type = bekeryType;
    products.price = 50;

    await productRepository.save(products);

    const product = await productRepository.find({ relations: { type: true } });
    console.log(product);
  })
  .catch((error) => console.log(error));
